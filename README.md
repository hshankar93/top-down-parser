# README #

An object-oriented top-down parser in Java that translates every TinyPL program into an
equivalent sequence of byte-codes for a Java Virtual Machine.

### Assumptions ###

1. All input test cases will be syntactically correct; syntax error-checking is not necessary.
2. The lexical analyzer only accepts an id with a single letter, and an int_lit that is an unsigned integer.
3. Follow Java byte-code naming convention for all opcodes.


### Program Structure ###

1. There should be one Java class definition for each nonterminal of the grammar. 
	Place the code for the top-down procedure in the class constructor itself.
2. There should be a top-level driver class called Parser and another class, called
	Code, which has methods for code generation.
3. The code for the lexical analyzer will be given to you